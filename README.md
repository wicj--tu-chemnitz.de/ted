# Ted
Ein Hybrid Testbed for Educational Purposes

Der Main Branch ist erst einmal pures Qemu. :)
# Bauen, tlw. aus Qemu-Doku: 
1. Switch to the QEMU root directory.
cd qemu
2. Configure QEMU (--enable-debug is optional).
./configure --target-list=avr-softmmu --enable-debug
3. Start the build.
make

# Beispielhafte Ausführung:
./build/avr-softmmu/qemu-system-avr -M uno -bios <Testprogramm> -serial telnet:localhost:4321,server,nowait -nographic
--> Startet eine ATmega328P MCU. Geladen wird das Testprogramm. Der Qemu Monitor wird durch -nographic direkt im Terminal ausgeführt. Über Telnet in einem weiteren Terminal kann die serielle Kommunikation stattfinden.
